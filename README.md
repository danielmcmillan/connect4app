# Connect4App

Connect 4 React application with AI.

Uses [Connect 4 AI implemented in C++](https://bitbucket.org/danielmcmillan/connect4ai) and compiled to WebAssembly using Emscripten.
